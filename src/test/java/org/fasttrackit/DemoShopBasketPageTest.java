package org.fasttrackit;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.pages.BasketPage;
import org.fasttrackit.pages.CheckoutInfoPage;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.product.Product;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Feature("Basket page Tests")
public class DemoShopBasketPageTest {
    private final MainPage mainPage = new MainPage();
    private final Product product = new Product("1");
    private final BasketPage basketPage = new BasketPage();
    private final CheckoutInfoPage checkoutInfoPage = new CheckoutInfoPage();

    @BeforeMethod
    public void setup() {
        mainPage.clickOnTheLogoIcon();
    }

    @AfterMethod
    public void cleanUp() {
        mainPage.getFooter().resetPageState();
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void addToBasketTest() {
        product.addProductToBasket();
        assertEquals("1", mainPage.getHeader().numberOfProductsInBasket(),
                "After adding one product to basket, the basket counter is 1.");
    }


    @Test
    @Severity(SeverityLevel.NORMAL)
    public void increaseTheProductQuantityByAddingTheProductToBasketMultipleTimesTest() {
        product.addProductToBasket();
        product.addProductToBasket();
        product.addProductToBasket();
        assertEquals("3", mainPage.getHeader().numberOfProductsInBasket(),
                "After clicking three times on the 'Add to basket button', basket counter is 3.");
    }

    @Test
    public void continueShoppingFromBasketPageTest() {
        product.addProductToBasket();
        mainPage.getHeader().clickOnBasketButton();
        basketPage.clickOnContinueShoppingButton();
        String productListTitle = "Products";
        assertEquals(mainPage.getProductListTitle(), productListTitle,
                "After clicking on continue shopping button, the main page is displayed.");
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void goToCheckoutPageFromBasketPageTest() {
        product.addProductToBasket();
        mainPage.getHeader().clickOnBasketButton();
        basketPage.clickOnCheckoutButton();
        String checkoutInfoTitle = "Your information";
        assertEquals(checkoutInfoPage.getCheckoutInfoTitle(), checkoutInfoTitle,
                "After clicking on checkout button, checkout page is displayed.");

    }
}
