package org.fasttrackit;

import io.qameta.allure.Feature;
import org.fasttrackit.body.Modal;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.*;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Main page Tests")
public class DemoShopMainPageTest {

    MainPage mainPage = new MainPage();

    @BeforeMethod
    public void setup() {
        mainPage.clickOnTheLogoIcon();
    }

    @AfterMethod
    public void cleanUp() {
        mainPage.getFooter().resetPageState();
    }

    @Test
    public void verifyWebsiteTitleTest() {
        String expectedTitle = "Demo shop";
        String actualTitle = mainPage.appTitle();
        assertEquals(actualTitle, expectedTitle, "Application title is expected to be Demo shop.");
    }

    @Test
    public void verifyDemoShopFooterBuildDateDetailsTest() {
        String expectedFooterDetails = "Demo Shop | build date 2021-05-21 14:04:30 GTBDT";
        String actualFooterDetails = mainPage.getFooter().getDetails();
        assertEquals(actualFooterDetails, expectedFooterDetails);

    }

    @Test
    public void verifyQuestionIconIsDisplayedTest() {
        boolean questionMarkIsDisplayed = mainPage.getFooter().isQuestionIconDisplayed();
        assertTrue(questionMarkIsDisplayed, "Question mark is displayed.");
    }

    @Test
    public void clickOnAndCloseQuestionMarkTest() {
        mainPage.getFooter().clickOnQuestionIcon();
        Modal footerModal = new Modal();
        assertTrue(footerModal.isModalDisplayed(), "Question modal is displayed.");
        footerModal.clickOnCloseButton();
    }

    @Test
    public void verifyResetIconIsDisplayedTest(){
        boolean resetIconDisplayed = mainPage.getFooter().resetIconIsDisplayed();
        assertTrue(resetIconDisplayed, "Reset icon is displayed on main page. ");
    }



}
