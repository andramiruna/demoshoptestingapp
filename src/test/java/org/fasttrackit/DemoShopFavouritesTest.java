package org.fasttrackit;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.product.Product;
import org.testng.annotations.*;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

@Feature("Favourites Tests")
public class DemoShopFavouritesTest {
    private final MainPage mainPage = new MainPage();
    private final Product product = new Product("1");

    @BeforeMethod
    public void setup() {
        mainPage.clickOnTheLogoIcon();
    }

    @AfterMethod
    public void cleanUp() {
        mainPage.getFooter().resetPageState();
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void addProductToFavouritesTest() {
        product.addToFavouritesList();
        assertEquals("1", mainPage.getHeader().numberOfProductsInFavourites(),
                "By adding one product to favourites, favourites counter is 1.");

    }

    @Test
    public void verifyAddedProductIsInFavouritesTest() {
        product.addToFavouritesList();
        mainPage.getHeader().clickOnFavouritesButton();
        assertTrue(product.exists(),
                "By adding a product favourites, product is displayed in favourites page.");

    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    public void addProductToBasketFromFavouritesTest() {
        product.addToFavouritesList();
        mainPage.getHeader().clickOnFavouritesButton();
        product.addProductToBasket();
        assertEquals("1", mainPage.getHeader().numberOfProductsInBasket(),
                "After clicking on add to basket from wishlist page, basket counter is 1.");

    }

    @Test
    public void verifyIfProductAddedFromFavouritesIsAddedToBasketTest() {
        product.addToFavouritesList();
        mainPage.getHeader().clickOnFavouritesButton();
        product.addProductToBasket();
        assertEquals("1", mainPage.getHeader().numberOfProductsInBasket(),
                "After adding the product from favourites to basket, basket counter is 1.");

    }
}

