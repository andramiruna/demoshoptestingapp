package org.fasttrackit;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.pages.*;
import org.fasttrackit.product.Product;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Checkout Tests")
public class DemoShopCheckoutTest {
    private final MainPage mainPage = new MainPage();
    private final Product product = new Product("1");
    private final BasketPage basketPage = new BasketPage();
    private final CheckoutInfoPage checkoutInfo = new CheckoutInfoPage();
    private final CheckoutSummaryPage checkoutSummary = new CheckoutSummaryPage();
    private final CheckoutCompletePage checkoutComplete = new CheckoutCompletePage();

    @BeforeMethod
    public void setup() {
        mainPage.clickOnTheLogoIcon();
    }

    @AfterMethod
    public void cleanUp() {
        mainPage.getFooter().resetPageState();
    }


    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void checkoutWithCompleteInfoTest() {
        product.addProductToBasket();
        mainPage.getHeader().clickOnBasketButton();
        basketPage.clickOnCheckoutButton();
        checkoutInfo.typeFirstName("Jane");
        checkoutInfo.typeLastName("Doe");
        checkoutInfo.typeAddress("Address");
        checkoutInfo.clickOnContinueCheckoutButton();
        String checkoutSummaryTitle = "Order summary";
        assertEquals(checkoutSummary.getCheckoutSummaryTitle(), checkoutSummaryTitle,
                "After information fields are completed and checkout button is clicked, checkout summary page is displayed.");
    }


    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void purchaseCompletedTest() {
        product.addProductToBasket();
        mainPage.getHeader().clickOnBasketButton();
        basketPage.clickOnCheckoutButton();
        checkoutInfo.typeFirstName("Jane");
        checkoutInfo.typeLastName("Doe");
        checkoutInfo.typeAddress("Address");
        checkoutInfo.clickOnContinueCheckoutButton();
        checkoutSummary.clickOnCompleteYourOrderButton();
        String checkoutCompleteTitle = "Order complete";
        assertEquals(checkoutComplete.getCheckoutCompleteTitle(), checkoutCompleteTitle,
                "After clicking on complete your order button, checkout complete page and thank you message"
                        + checkoutComplete.getOrderPlacedMessage() +  "are displayed ");
        checkoutComplete.clickOnContinueShoppingButton();
        String productListTitle = "Products";
        assertEquals(mainPage.getProductListTitle(), productListTitle,
                "After continue shopping button is clicked, main page is displayed.");
    }
}
