package org.fasttrackit;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.body.Modal;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.*;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

@Feature("Login Tests")
public class DemoShopLoginTest {

    MainPage mainPage = new MainPage();

    @BeforeMethod
    public void setup() {
        mainPage.clickOnTheLogoIcon();
    }

    @AfterMethod
    public void cleanUp() {
        mainPage.getFooter().resetPageState();
    }

    @Test
    public void clickOnTheLoginButtonTest() {
        mainPage.clickOnTheLoginButton();
        Modal loginModal = new Modal();
        boolean loginModalIsDisplayed = loginModal.isModalDisplayed();
        assertTrue(loginModalIsDisplayed, "Login modal is displayed after login button is clicked.");
        loginModal.clickOnCloseButton();
    }

    @Test
    public void getLoginModalTitleTest() {
        mainPage.clickOnTheLoginButton();
        Modal loginModal = new Modal();
        String actualModalTitle = loginModal.getLoginModalTitle();
        String expectedModalTitle = "Login";
        assertEquals(actualModalTitle, expectedModalTitle, "Login modal title is 'Login'.");
        loginModal.clickOnCloseButton();
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void loginWithValidCredentialsTest() {
        mainPage.getHeader().clickOnLoginIcon();
        Modal loginModal = new Modal();
        assertTrue(loginModal.isModalDisplayed(), "Login modal is displayed after login icon is clicked.");
        loginModal.typeInUsername("dino");
        loginModal.typeInPassword("choochoo");
        loginModal.clickOnTheLoginButton();
        String expectedGreetingsMessage = "Hi dino!";
        assertEquals(mainPage.getHeader().getGreetingsMessage(),expectedGreetingsMessage, "After Login the Welcome message contains dino.");
        mainPage.getHeader().clickOnLogoutIcon();
        sleep(2000);
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void demoShopSignOutTest() {
        if (mainPage.getHeader().isLoggedOut()) {
            mainPage.getHeader().clickOnLoginIcon();
            Modal loginModal = new Modal();
            loginModal.typeInUsername("dino");
            loginModal.typeInPassword("choochoo");
            loginModal.clickOnTheLoginButton();
        }
        assertTrue(mainPage.getHeader().isLogoutIconDisplayed(), "After login, logout button is displayed on home page.");
        mainPage.getHeader().clickOnLogoutIcon();
        String expectedGuestGreetingsMsg = "Hello guest!";
        assertEquals(mainPage.getHeader().getGreetingsMessage(), expectedGuestGreetingsMsg,
                "After logout button is clicked welcome message is: 'Hello guest!'");
    }


    @Test
    public void loginWithPartiallyValidUsernameAndValidPasswordTest() {
        mainPage.getHeader().clickOnLoginIcon();
        Modal loginModal = new Modal();
        assertTrue(loginModal.isModalDisplayed(), "Login modal is displayed after login button is clicked.");
        loginModal.typeInUsername("turtle1");
        loginModal.typeInPassword("choochoo");
        loginModal.clickOnTheLoginButton();
        String expectedErrorMsg = "Incorrect username or password!";
        assertEquals(loginModal.getErrorMsg(), expectedErrorMsg, "Error message is displayed after login button is clicked: "
                + expectedErrorMsg);
        loginModal.clickOnCloseButton();
    }
    @Test
    public void loginWithValidUsernameAndPartiallyValidPasswordTest(){
        mainPage.getHeader().clickOnLoginIcon();
        Modal loginModal = new Modal();
        assertTrue(loginModal.isModalDisplayed(), "Login modal is displayed after login button is clicked.");
        loginModal.typeInUsername("turtle");
        loginModal.typeInPassword("choochoo2");
        loginModal.clickOnTheLoginButton();
        String expectedErrorMsg = "Incorrect username or password!";
        assertEquals(loginModal.getErrorMsg(), expectedErrorMsg, "Error message is displayed after login button is clicked: "
                + expectedErrorMsg);
        loginModal.clickOnCloseButton();
    }
    @Test
    public void loginWithPartiallyValidUsernameAndPartiallyValidPasswordTest(){
        mainPage.getHeader().clickOnLoginIcon();
        Modal loginModal = new Modal();
        assertTrue(loginModal.isModalDisplayed(), "Login modal is displayed after login button is clicked.");
        loginModal.typeInUsername("turtle3");
        loginModal.typeInPassword("choochoo3");
        loginModal.clickOnTheLoginButton();
        String expectedErrorMsg = "Incorrect username or password!";
        assertEquals(loginModal.getErrorMsg(), expectedErrorMsg, "Error message is displayed after login button is clicked: "
                + expectedErrorMsg);
        loginModal.clickOnCloseButton();
    }

    @Test
    public void loginWithNoUsernameAndValidPasswordTest(){
        mainPage.getHeader().clickOnLoginIcon();
        Modal loginModal = new Modal();
        assertTrue(loginModal.isModalDisplayed(), "Login modal is displayed after login button is clicked.");
        loginModal.typeInUsername("");
        loginModal.typeInPassword("choochoo");
        loginModal.clickOnTheLoginButton();
        String expectedErrorMsg = "Please fill in the username!";
        assertEquals(loginModal.getErrorMsg(), expectedErrorMsg, "Error message is displayed after login button is clicked: "
                + expectedErrorMsg);
        loginModal.clickOnCloseButton();
    }

    @Test
    public void loginWithNoUsernameAndPartiallyValidPasswordTest(){
        mainPage.getHeader().clickOnLoginIcon();
        Modal loginModal = new Modal();
        assertTrue(loginModal.isModalDisplayed(), "Login modal is displayed after login button is clicked.");
        loginModal.typeInUsername("");
        loginModal.typeInPassword("choochoo5");
        loginModal.clickOnTheLoginButton();
        String expectedErrorMsg = "Please fill in the username!";
        assertEquals(loginModal.getErrorMsg(), expectedErrorMsg, "Error message is displayed after login button is clicked: "
                + expectedErrorMsg);
        loginModal.clickOnCloseButton();
    }

    @Test
    public void loginWithValidUsernameAndNoPasswordTest(){
        mainPage.getHeader().clickOnLoginIcon();
        Modal loginModal = new Modal();
        assertTrue(loginModal.isModalDisplayed(), "Login modal is displayed after login button is clicked.");
        loginModal.typeInUsername("turtle");
        loginModal.typeInPassword("");
        loginModal.clickOnTheLoginButton();
        String expectedErrorMsg = "Please fill in the password!";
        assertEquals(loginModal.getErrorMsg(), expectedErrorMsg, "Error message is displayed after login button is clicked: "
                + expectedErrorMsg);
        loginModal.clickOnCloseButton();
    }

    @Test
    public void loginWithPartiallyValidUsernameAndNoPasswordTest(){
        mainPage.getHeader().clickOnLoginIcon();
        Modal loginModal = new Modal();
        assertTrue(loginModal.isModalDisplayed(), "Login modal is displayed after login button is clicked.");
        loginModal.typeInUsername("turtle5");
        loginModal.typeInPassword("");
        loginModal.clickOnTheLoginButton();
        String expectedErrorMsg = "Please fill in the password!";
        assertEquals(loginModal.getErrorMsg(), expectedErrorMsg, "Error message is displayed after login button is clicked: "
                + expectedErrorMsg);
        loginModal.clickOnCloseButton();
    }

    @Test
    public void loginWithoutUsernameAndWithoutPasswordTest(){
        mainPage.getHeader().clickOnLoginIcon();
        Modal loginModal = new Modal();
        assertTrue(loginModal.isModalDisplayed(), "Login modal is displayed after login button is clicked.");
        loginModal.typeInUsername("");
        loginModal.typeInPassword("");
        loginModal.clickOnTheLoginButton();
        String expectedErrorMsg = "Please fill in the username!";
        assertEquals(loginModal.getErrorMsg(), expectedErrorMsg, "Error message is displayed after login button is clicked: "
                + expectedErrorMsg);
        loginModal.clickOnCloseButton();
    }





}
