package org.fasttrackit.product;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Product {
    private final SelenideElement productLink;
    private final SelenideElement card;
    private final SelenideElement addToBasketButton;
    private final SelenideElement addToFavouritesButton;
    public Product(String productId) {
        this.productLink = $(String.format("[href='#/product/%s']", productId));
        this.card = productLink.parent().parent();
        this.addToBasketButton = card.$(".fa-cart-plus");
        this.addToFavouritesButton = card.$(".fa-heart");
    }

    public void addProductToBasket() {
        this.addToBasketButton.click();
    }

    public void addToFavouritesList() {
        this.addToFavouritesButton.click();
    }

    public boolean exists() {
        return this.productLink.exists();
    }

    @Override
    public String toString() {
        return this.productLink.text();
    }

}
