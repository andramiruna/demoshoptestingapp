package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Modal {
    private final SelenideElement loginModalTitle = $(".modal-title");
    private final SelenideElement modal = $(".modal-dialog");
    private final SelenideElement closeButton = $(".close");
    private final SelenideElement user = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement loginButton = $("button.btn.btn-primary");
    private final SelenideElement errorMsg = $("p.error");

    public String getLoginModalTitle() {
        return loginModalTitle.getText();
    }

    public void clickOnCloseButton() {
        System.out.println("Clicked on the 'x' button.");
        if (modal.isDisplayed())
            closeButton.click();
    }

    public boolean isModalDisplayed() {
        return modal.isDisplayed();
    }

    public void validateModalComponents() {
        System.out.println("1. Verify that the modal is displayed.");
        System.out.println("2. Verify that the modal title is : " + loginModalTitle);
        System.out.println("3. Verify that the modal close button is : " + closeButton);
        System.out.println("4. Verify that the username field is displayed.");
        System.out.println("5. Verify that the password field is displayed.");
        System.out.println("6. Verify that the login button is displayed.");
        System.out.println("7. Verify that the login button is enabled.");
    }

    public void typeInUsername(String userToType) {
        System.out.println("Typed in " + userToType);
        user.sendKeys(userToType);
    }

    public void typeInPassword(String passwordToType) {
        System.out.println("Typed in " + passwordToType);
        password.sendKeys(passwordToType);
    }

    public void clickOnTheLoginButton() {
        loginButton.click();
        System.out.println("Clicked on the login button.");
    }

    public String getErrorMsg(){
        return errorMsg.getText();
    }

    public void clickOnUsernameField() {
    }
}
