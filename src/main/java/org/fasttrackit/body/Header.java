package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Header {

    private final SelenideElement logoIcon = $(".navbar-brand .brand-logo");
    private final SelenideElement loginIcon = $(".fa-sign-in-alt");
    private final SelenideElement logOutIcon = $("[data-icon=sign-out-alt]");
    private final SelenideElement greetingsMessage = $(".navbar-text span span");
    private final SelenideElement favouritesIcon = $("[href='#/wishlist']");
    private final SelenideElement basketIcon = $("[href='#/cart']");
    private final SelenideElement favouritesNumber = favouritesIcon.$(".shopping-cart-icon .fa-heart~span");
    private final SelenideElement basketNumber = basketIcon.$(".shopping-cart-icon .fa-shopping-cart~span");

    public String getGreetingsMessage() {
        return greetingsMessage.getText();
    }

    public void clickOnLoginIcon() {
        loginIcon.scrollTo();
        loginIcon.click();
    }

    public void clickOnLogoutIcon() {
        logOutIcon.scrollTo();
        logOutIcon.click();
    }

    public boolean isLoggedOut() {
        greetingsMessage.scrollTo();
        return greetingsMessage.getText().contains("guest");
    }

    public boolean isLogoutIconDisplayed() {
        return logOutIcon.isDisplayed();
    }

    public String numberOfProductsInFavourites() {
        if (favouritesNumber.exists())
            return favouritesNumber.getText();
        return "";
    }

    public void clickOnFavouritesButton() {
        favouritesIcon.click();
    }

    public void clickOnBasketButton() {
        basketIcon.click();
    }

    public String numberOfProductsInBasket() {
        if (basketNumber.exists())
            return basketNumber.getText();
        return "";
    }

    public void clickOnLogoIcon() {
        this.logoIcon.scrollTo();
        this.logoIcon.click();
    }




}
