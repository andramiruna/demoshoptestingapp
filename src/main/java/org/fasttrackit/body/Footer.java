package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Footer {
    private final SelenideElement details = $(".nav-link");

    private final SelenideElement questionIcon = $("[data-icon=question]");

    private final SelenideElement resetIcon = $("[data-icon=undo]");



    public String getDetails() {
        return this.details.getText();
    }

    public boolean isQuestionIconDisplayed() {
        return questionIcon.isDisplayed();
    }

    public void clickOnQuestionIcon() {
        questionIcon.click();
    }


    public boolean resetIconIsDisplayed() {
        return resetIcon.isDisplayed();
    }

    public void resetPageState() {
        this.resetIcon.scrollTo();
        this.resetIcon.click();
    }

}
