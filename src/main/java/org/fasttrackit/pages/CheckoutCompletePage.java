package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutCompletePage {
    private final SelenideElement checkoutCompleteTitle = $(".subheader-container .text-muted");
    private final SelenideElement orderPlacedMessage = $(".text-center");
    private final SelenideElement continueShoppingButton = $(".btn.btn-success");

    public String getCheckoutCompleteTitle() {
        return checkoutCompleteTitle.text();
    }

    public String getOrderPlacedMessage() {
        return orderPlacedMessage.text();
    }

    public void clickOnContinueShoppingButton() {
        continueShoppingButton.click();
    }
}
