package org.fasttrackit.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.body.Footer;
import org.fasttrackit.body.Header;

import static com.codeborne.selenide.Selenide.$;


public class MainPage extends Page {

    private String title = Selenide.title();

    private final SelenideElement productListTitle = $(".subheader-container .text-muted");
    private final Footer footer;

    private Header header;

    public MainPage() {
        System.out.println("Constructing header.");
        this.header = new Header();
        System.out.println("Constructing footer.");
        this.footer = new Footer();
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Header getHeader() {
        return this.header;
    }

    public Footer getFooter() {
        return this.footer;
    }

    public String appTitle() {
        System.out.println("1. Verify that title is : " + title);
        return title;
    }

    public void clickOnTheLoginButton() {
        this.header.clickOnLoginIcon();
    }


    public void clickOnTheLogoIcon() {
        this.header.clickOnLogoIcon();
        System.out.println("Return to homepage.");
    }

    public String getProductListTitle() {
        productListTitle.scrollTo();
        return productListTitle.text();
    }

}
