package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutInfoPage {

    private final SelenideElement checkoutInfoTitle = $(".subheader-container");
    private final SelenideElement firstName = $("#first-name.form_input.form-control");
    private final SelenideElement lastName = $("#last-name.form_input.form-control");
    private final SelenideElement address = $("#address.form_input.form-control");
    private final SelenideElement cancelButton = $(".btn.btn-danger");
    private final SelenideElement continueCheckoutButton = $(".btn.btn-success");
    private final SelenideElement errorMessage = $(".error");

    public String getCheckoutInfoTitle() {
        checkoutInfoTitle.scrollTo();
        return checkoutInfoTitle.text();
    }
    public void typeFirstName(String input) {
        firstName.sendKeys(input);
    }
    public void typeLastName(String input) {
        lastName.sendKeys(input);
    }
    public void typeAddress(String input) {
        address.sendKeys(input);
    }
    public void clickOnCancelButton() {
        cancelButton.click();
    }
    public void clickOnContinueCheckoutButton() {
        continueCheckoutButton.click();
    }

    public String getErrorMessage() {
        errorMessage.scrollTo();
        return errorMessage.text();
    }



}
