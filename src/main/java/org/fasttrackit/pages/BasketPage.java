package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class BasketPage {
    private final SelenideElement continueShoppingIcon = $(".btn.btn-danger");
    private final SelenideElement checkoutIcon = $(".btn.btn-success");


    public void clickOnContinueShoppingButton() {
        continueShoppingIcon.click();
    }

    public void clickOnCheckoutButton() {
        checkoutIcon.click();
    }
}
