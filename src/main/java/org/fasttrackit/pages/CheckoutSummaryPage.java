package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutSummaryPage {
    private final SelenideElement checkoutSummaryTitle = $(".subheader-container");

    private final SelenideElement completeOrderButton = $(".btn.btn-success");


    public String getCheckoutSummaryTitle() {
        checkoutSummaryTitle.scrollTo();
        return checkoutSummaryTitle.text();
    }

    public void clickOnCompleteYourOrderButton() {
        completeOrderButton.click();
    }
}
