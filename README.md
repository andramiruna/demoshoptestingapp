# Exam Project

## Titlul proiectului: DemoShopTestingApp
Link-ul proiectului : https://gitlab.com/andramiruna/demoshoptestingapp.git

### Website: https://fasttrackit-test.netlify.app/#/

## Descriere :
#### Demo shop este un site web demonstrativ care funcționează asemănator unui site de comerț electronic real, deosebirea fiind data de faptul că nu se pot onora comenzile plasate.
Website-ul Demo shop oferă utilizatorului următoarele posibilități:
- logare cu un cont creat;
- căutarea produselor după cuvinte cheie;
- sortarea produselor după nume și după preț, în ordine crescătoare și descrescătoare;
- adăugarea produselor în lista de favorite și în coșul de cumpărături;
- modificarea cantității produselor din coșul de cumpărături;
- eliminarea produselor din coșul de cumpărături şi din lista de favorite;
- finalizarea comenzii.

Toate aceste acţiuni sunt acceptate atât pentru utilizator înregistrat, cât şi pentru utilizator neînregistrat.

Prin suita de teste creată, am verificat funcționalități importante ale site-ului, legate de urmatoarele functii: Login si Logout, Basket, Favourites, Checkout, toate actiunile fiind realizate ca utilizator neînregistrat.

Pentru fiecare pagină testată, în directorul src/main/java a fost definită clasa corespunzătoare iar în directorul src/test/java au fost definite clasele de test corespunzătoare. 

## Tehnologiile utilizate în realizarea proiectului:

Limbaj de programare: Java (versiunea 17.0.6);
-	IDE (Integrated Development Environment):  IntelliJ IDEA 2022.2.3;
-   Selenide (versiunea 6.12.4): Framework utilizat pentru automatizarea testelor cu ajutorul Selenium WebDriver. Acest framework are o configurare simplă iar testele sunt mai scurte și mai ușor de citit.
-   TestNG (versiunea 7.7.1): Framework de testare utilizat pentru limbajul de programare Java, inspirat de JUnit. Obiectivul acestui framework este de a acoperi o gamă cât mai largă de teste: unit tests, functional, end-to-end etc. TestNg are o serie de funcționalități mai puternice și mai ușor de utilizat. Prin utilizarea acestui framework, se poate genera un raport corespunzător și se poate afla cu ușurință câte teste au trecut, au eșuat și care au fost omise.
-   Allure reports: Allure framework este un instrument flexibil și ușor de utilizat, fiind folosit în generarea rapoartelor pentru teste. Acest framework oferă o reprezentare foarte concisă a ceea ce a fost testat, sub forma unui raport web clar și permite tuturor celor implicați în procesul de dezvoltare să extragă maximum de informații utile din execuția testelor.
-   Apache Maven (versiunea 2.22.2): Este un instrument software bazat pe conceptul project object model (POM). Maven poate gestiona crearea, raportarea și documentația unui proiect bazat pe limbajul de programare Java;
-   Browser: Version 112.0.5615.50 (Official Build) (64-bit)